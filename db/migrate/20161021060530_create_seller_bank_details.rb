class CreateSellerBankDetails < ActiveRecord::Migration
  def change
    create_table :seller_bank_details do |t|
      t.string :bank_name
      t.string :account_number
      t.string :routing_number
      t.boolean :status, default: true
      t.references :user

      t.timestamps null: false
    end
  end
end
