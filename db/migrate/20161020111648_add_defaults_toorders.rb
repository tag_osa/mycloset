class AddDefaultsToorders < ActiveRecord::Migration
  def change
  	change_column :orders, :is_cashout_requested, :boolean, :default => false
  	change_column :orders, :sellers_earnings, :float, :default => 0
  	change_column :orders, :my_closet_earnings, :float, :default => 0
  end
end
