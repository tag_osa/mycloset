class CreateColorsProducts < ActiveRecord::Migration
  def change
    create_table :colors_products do |t|
      t.references :color
      t.references :product
      t.timestamps null: false
    end    
  end
end
