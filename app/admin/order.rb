ActiveAdmin.register Order do
  menu :priority => 6
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  config.batch_actions = false
  scope :joined, :default => true do |order|
    if (request.format == 'text/csv') && params[:cashout_id]
  	  order.includes(:product=>[:user]).where(:cashout_id=>params[:cashout_id])
    else
      order.includes(:product=>[:user])
    end
  end
  actions :all, except: [:new, :destroy, :edit]

  index do
    section :style => 'float:left;margin-right:20px;' do
      div do
          render "reports"
      end
    end unless params[:cashout_id]
  	# column :order_no
    column "Order Number", :sortable => 'orders.order_no' do |order|
      link_to order.order_no, admin_order_path(order)
    end
    column "Image", :input_html=> {:style => "width:30px;"} do |order|
      image_tag order.product.attachments.present? ? "http:"+order.product.attachments.first.attachment.url(:small) : "No Image", :style => "max-width:30px"
    end
  	column "Product Name", :product_id, :sortable => "products.headline" do |order|
        if order.try(:product).present?
        	link_to order.try(:product).try(:headline), admin_product_path(order.try(:product))
        else
        	"None"
        end
    end
  	column :amount
    column "Seller Earnings", :sortable => 'orders.sellers_earnings' do |order|
      order.sellers_earnings
    end if params[:report] || params[:cashout_id]

    column "MyCloset Earnings", :sortable => 'orders.my_closet_earnings' do |order|
      order.my_closet_earnings
    end if params[:report] || params[:cashout_id]
  	column "Seller", :seller_name, :sortable => 'users.uid' do |order|
      if order.try(:product).try(:user).present?
        link_to order.try(:product).try(:user).try(:uid), admin_user_path(order.try(:product).try(:user))
      else
      	"None"
      end
    end
    column "Buyer", :user_id, :sortable => 'users.uid' do |order|
        if order.try(:user).present?
          link_to order.try(:user).try(:uid), admin_user_path(order.try(:user))
        else
          "No Orders found"
        end
    end
    column :shipped_on
    column :returned_on
    column :transfer_id
    # column :payment_type unless params[:report]
    # column :payment_status, :sortable => 'orders.payment_status' do |order|
    #     status_tag (order.payment_status), ((order.payment_status == "succeeded") ? :ok : :error)
    # end unless params[:report]
    column "Order Status", :status
    column "Created On", :created_at, :sortable => "orders.created_at" do |order|
        order.created_at.strftime("%d %B %Y")
    end if params[:report]

  	# actions unless params[:report]
  end

  csv do
    column("Order No")  { |order| order.try(:order_no) }
    column("Product Name")  { |order| order.try(:product).present? ? order.try(:product).try(:headline) : "None" }
    column("Amount")  { |order| order.try(:amount) }
    column("Seller Earnings")  { |order| order.try(:sellers_earnings) } if params[:report] || params[:cashout_id]
    column("MyCloset Earnings")  { |order| order.try(:my_closet_earnings) } if params[:report] || params[:cashout_id]
    column("Seller")  { |order| order.try(:product).try(:user).present? ? order.try(:product).try(:user).try(:uid) : "None" }
    column("Buyer")  { |order| order.try(:user).try(:uid) }
    column("Payment type")  { |order| order.try(:payment_type) } unless params[:report]
    column("Payment Status")  { |order| order.try(:payment_status) } unless params[:report]
    column("Order Status")  { |order| order.try(:status) }
    column("Created On")  { |order| order.try(:created_at) } if params[:report]
  end
  
  filter :product_id, as: :Select, collection: ActiveRecord::Base.connection.table_exists?('products') ? Product.where("id > ?",0).pluck(:headline, :id) : []
  filter :amount
  filter :sellers_earnings, :label=>"Seller Earnings",  :if=> proc{params[:report] || params[:cashout_id]}
  filter :my_closet_earnings, :label=>"MyCloset Earnings",  :if=> proc {params[:report] || params[:cashout_id]}
  # filter :seller_name, as: :Select, collection: ActiveRecord::Base.connection.table_exists?('users') ? User.where(:id=>Product.pluck(:user_id)).pluck(:uid, :id) : [], :label=>"Seller Name"
  filter :user, as: :Select, collection: ActiveRecord::Base.connection.table_exists?('users') ? User.where("id > ?",0).pluck(:uid, :id) : [], :label=>"Buyer"
  filter :payment_type,  :unless=> proc{params[:report]}
  filter :payment_status,  :unless=> proc{params[:report]}
  filter :status, :label=>"Order Status"
  filter :created_at, :label=>"Created On", :if=> proc{params[:report]}

  show do
    panel "Purchase Details" do
      attributes_table_for order do
      	row :order_no
      	row("Transaction Id") {
      		order.try(:transcation_id)
      	}
      	row("Product Name") {
        	link_to order.try(:product).try(:headline), admin_product_path(order.try(:product))
        }
        row :amount
        row("Seller Name") {
        	link_to order.try(:product).try(:user).try(:uid), admin_user_path(order.try(:product).try(:user))
        }
        row("Buyer Name") {
        	link_to order.try(:user).try(:uid), admin_user_path(order.try(:user))
        }
        row :payment_type
        row :payment_status
        row :status
        row :item_quality_rating
        row :shipping_speed_rating
      end
  	end
  end

  controller do
    def index
        super do |format|
          if params[:cashout_id].present?
            per_page = (request.format == 'text/html') ? 30 : 10_000
            params[:page] = nil unless (request.format == 'text/html')
            @orders = @orders.where(:cashout_id=>params[:cashout_id]) #.page(params[:page]).per(per_page) #if !params[:commit].present? && params[:order] == ""
            @orders ||= end_of_association_chain.paginate if @orders.present?
          end
        end
    end
  end

  # permit_params :user_id, :shipping_address, :is_receive_email_receipt
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


end
