class Api::V3::NotificationsController < ApplicationController
	include DeviseTokenAuth::Concerns::SetUserByToken
	before_action :authenticate_api_v1_user!

	def initialize
		@limit = 20
	end

	#This method is to get private messages by user
	def index
	    params.has_key? (:offset) ? offset_count = params[:offset].to_i : offset_count = 0
		 begin
			data_array = []
			notifications = \
			current_api_v1_user.notifications.limit(@limit).offset(offset_count).select(:id,:message_type,:message, :product_id, :status,:created_at).order('created_at DESC')
			notifications.update_all(:status => true)
			notifications.each do |notification|
				user_image_url = ""
				data_hash = {
					:id => notification.id,
					:message_type => notification.message_type,
					:message => notification.message, 
					:user_image_url => notification.try(:user).try(:attachment).nil? ? "" : 'http:' + notification.user.attachment.attachment.url(:small),
					:product_id => notification.product_id, 
					:status => notification.status,
					:created_at => notification.created_at,
					:order_id => ""
				}				
				if(notification.message_type == "order") 
					product = Product.find_by_id(notification.product_id)
					order_id = product.order.try(:id)
					data_hash["order_id"] = order_id
				end
				data_array << data_hash
			end
			render success_response_with_object("sucess",200,data_array)
		rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end		
	end

	private

	def notification_message_params
		params.require(:notifications).permit(
			:message_type,
			:message,
			:status,
			:sender_id,
			:user_id,
			:product_id
			)
	end
end
