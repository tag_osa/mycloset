class UserMailer < ActionMailer::Base
    include Devise::Mailers::Helpers
    default from: "myKlosets <admin@myklosets.com>"
    def reset_password_instructions(resource, opts={})
        @resource = resource
        @resource.update_attributes(:reset_password_token => Random.rand(100000...9999999))
        mail(to: @resource.email, subject: "Reset Password Instructions")
    end
    
    def send_welcome_email(resource, opts={})
        @user_name = resource.name || resource.uid
        @resource = resource
        mail(to: @resource.email, subject: "Welcome to myKlosets")
    end
end