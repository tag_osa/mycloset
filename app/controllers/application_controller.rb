require "push_notifications_bulk_import.rb"
require 'gcm'
require 'fcm'

class ApplicationController < ActionController::Base

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  # skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }

  CHARGES = YAML.load_file("config/constants.yml")[Rails.env]

   # This method will give the error response to client.
  def error_response(message, code)
    return :json => {:status => false, :message => message, :code => code}
  end

  # This method will give the success response to client.
  def success_response(message, code)
    return :json => {:status => true, :message => message, :code => code}
  end
  
  # This method will give the success message with object response to client.
  def success_response_with_object(message, code, json_object)
    return :json => {:status => true, :message => message, :code => code, :data => json_object}
  end

  # This method will give the success message with object response to client.
  def success_response_with_object_sales(message, code, json_object, cash_out_amount, cash_out_orders)
    return :json => {:status => true, :message => message, :code => code, :data => json_object, :cash_out_amount => cash_out_amount, :cash_out_orders => cash_out_orders}
  end

  # This method used for define new hash
  def new_hash
    return Hash.new
  end

  # This method will give the success message with object response to client.
  def success_response_with_object_product(message, code,
    json_object, attachments=nil, colors=nil, wishes=nil, comments=nil, product_love_id=nil)
    return :json => {
      :status => true,
      :message => message,
      :code => code,
      :data => json_object,
      :attachments => attachments,
      :colors => colors,
      :product_loves_count => wishes,
      :comments => comments,
      :product_love_id => product_love_id
    }
  end


  # This method will give the error message with object response to client.
  def error_response_with_object(message, code, json_object)
    return :json => {:status => false, :code => code, :message => message, :data => json_object}
  end

  #To Send Push Notification to andrioid users 
  def android_push_notification(user_reg_ids,message,title,body)
    fcm = FCM.new(CONFIG["new_fcm_key"])
    # gcm = GCM.new(CONFIG["fcm_key"])
    registration_ids= user_reg_ids # an array of one or more client registration tokens
    options = { 
      priority: 'high', # Optional, can be range 5 to 10
      data: { message: message.gsub("'", %q(\\\'))  },
      notification: { body: message.gsub("'", %q(\\\')) ,title: title,sound: "default"}
    }
    # response = gcm.send(registration_ids, options)
    response = fcm.send(registration_ids, options)
  end

  #To Send Push Notification to ios users
  def ios_push_notification(user_reg_ids,message,title,body)
    # gcm = GCM.new(CONFIG["fcm_key"])
    fcm = FCM.new(CONFIG["new_fcm_key"])
    registration_ids= user_reg_ids # an array of one or more client registration tokens
    options = { 
      priority: 10, # Optional, can be range 5 to 10
      data: { message: message.gsub("'", %q(\\\'))  },
      notification: { body: message.gsub("'", %q(\\\')) ,title: title,sound: "default"}
    }
    # response = gcm.send(registration_ids, options)
    response = fcm.send(registration_ids, options)

  end


  #To get price amount stripe charges and total amount to pay
  def get_amount_with_stripe_charges(amount)
      total_amt = (amount + CONFIG["stripe_charges_additional_cent"])/(1-(CONFIG["stripe_charges_in_percent"]/100))
      return total_amt.round(2), (total_amt - amount).round(2)
  end

  #To active get offer
  def get_all_offers
    offers = Offer.where(:status => true).try(:first)
  end

  # This method used for update the notification table
  def update_notification(type=nil, product, sender, message)
      Notification.create(
          :message_type => type,
          :message => message,
          :status => false,
          :user_id => product.user.try(:id),
          :product_id => product.id,
          :sender_id => sender.id
      )
  end

end
