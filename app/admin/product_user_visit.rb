ActiveAdmin.register ProductUserVisit do

menu :priority => 11
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
# 
  scope :joined, :default => true do |product_user_visit|
  	product_user_visit.includes(:user, :product=>[:wishlists,:item_comments])
  end
  actions :all, except: [:edit, :show, :new, :destroy]

  index do
    column "Image", :input_html=> {:style => "width:30px;"} do |product_user_visit|
      image_tag product_user_visit.try(:product).attachments.present? ? "http:"+product_user_visit.try(:product).attachments.first.attachment.url(:small) : "No Image", :style => "max-width:30px"
    end
    column "Headline" do |product_user_visit|
      product_user_visit.try(:product).try(:headline)
    end
    column :visits
	column "Loves", :input_html=> {:style => "width:30px;"} do |product_user_visit|
      product_user_visit.try(:product).try(:wishlists).try(:length)
    end
    column "Comments", :input_html=> {:style => "width:30px;"} do |product_user_visit|
      product_user_visit.try(:product).try(:item_comments).try(:length)
    end
  end

  csv do
    column("Headline")  { |product_user_visit| product_user_visit.try(:product).try(:headline) }
    column("Visits")  { |product_user_visit| product_user_visit.try(:visits) }
    column("Loves")  { |product_user_visit| product_user_visit.try(:product).try(:wishlists).try(:length) }
    column("Comments")  { |product_user_visit| product_user_visit.try(:product).try(:item_comments).try(:length) }
  end

  filter :product, as: :Select, collection: ActiveRecord::Base.connection.table_exists?('products') ? Product.where("id > ?",0).pluck(:headline, :id) : []  
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


end
