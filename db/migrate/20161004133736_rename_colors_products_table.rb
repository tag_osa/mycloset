class RenameColorsProductsTable < ActiveRecord::Migration
  def change
  	rename_table :colors_products, :color_products
  end
end
