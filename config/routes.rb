Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  namespace :api do
    ###################API Vesrion 1.0 Routes Starts Here###################
    namespace :v1 do
      #mount_devise_token_auth_for 'User', at: 'auth'
      mount_devise_token_auth_for 'User', at: 'auth', controllers: {
		  #passwords:          'api/v1/devise_token_auth/passwords',
		  registrations:      'api/v1/devise_token_auth/registrations',
		  #sessions:           'api/v1/devise_token_auth/sessions'
	  }

      get '/home' => 'home#index'

      resources :products do
        collection do
          get 'filters'
          get 'sorting'
          get 'search_by_query'          
        end
        member do
          get 'other_user_closet'
        end
      end

      resources :product_loves
      resources :orders do
        collection do
          post '/return' => 'orders#product_return'
        end
        member do
          post 'product_not_received'
        end
      end
      resources :notifications
      resources :private_messages
      resources :item_comments
      resources :donation_bags
      resources :cashouts
      resources :bank_details
      resources :subscriptions
      resources :profiles do
        member do
          post 'update_password'
          post 'update_return_policy'
          get 'fetch_return_policy'
          post 'update_profile_image'
        end
      end
      get '/types' => 'home#get_types'
      get '/fabrics' => 'home#get_fabrics'
      get '/conditions' => 'home#get_conditions'
      get '/cups' => 'home#get_cups'
      get '/size' => 'home#get_size'
      get '/colors' => 'home#get_colors'
      get '/default_values' => 'home#get_default_values'
      get '/my_closet' => 'products#my_closet'
      get '/sales' => 'orders#my_sales'
      get '/get_address' => 'profiles#get_address'
      get '/get_subscription_and_charges' => 'home#get_subscription_and_charges'

      post '/add_address' => 'profiles#add_address'
      post '/send-reset-password' => 'home#send_reset_password'
      post '/reset-password' => 'home#reset_password'

      post '/gcm-register' => 'home#gcm_register'

      post '/purchase_confirmation' => 'products#purchase_confirmation'
      post '/update_stripe' => 'home#update_stripe'

    end
    ###################API version 1.0 Routes Ends Here###################

    ###################API Vesrion 2.0 Routes Starts Here###################
    namespace :v2 do
      #mount_devise_token_auth_for 'User', at: 'auth'
      mount_devise_token_auth_for 'User', at: 'auth', controllers: {
      #passwords:          'api/v2/devise_token_auth/passwords',
      registrations:      'api/v2/devise_token_auth/registrations',
      #sessions:           'api/v2/devise_token_auth/sessions'
    }

      get '/home' => 'home#index'

      resources :products do
        collection do
          get 'filters'
          get 'sorting'
          get 'search_by_query'
          delete '/delete_attachment' => 'products#delete_attachment'
        end
        member do
          get 'other_user_closet'
          put 'set_primary_product_image'
        end
      end

      resources :product_loves
      resources :orders do
        collection do
          post '/return' => 'orders#product_return'
        end
        member do
          post 'product_not_received'
        end
      end
      resources :notifications
      resources :private_messages
      resources :item_comments
      resources :donation_bags
      resources :cashouts
      resources :bank_details
      resources :subscriptions
      resources :profiles do
        member do
          post 'update_password'
          post 'update_return_policy'
          get 'fetch_return_policy'
          post 'update_profile_image'
        end
      end
      get '/types' => 'home#get_types'
      get '/fabrics' => 'home#get_fabrics'
      get '/conditions' => 'home#get_conditions'
      get '/cups' => 'home#get_cups'
      get '/size' => 'home#get_size'
      get '/colors' => 'home#get_colors'
      get '/default_values' => 'home#get_default_values'
      get '/my_closet' => 'products#my_closet'
      get '/sales' => 'orders#my_sales'
      get '/get_address' => 'profiles#get_address'
      get '/get_subscription_and_charges' => 'home#get_subscription_and_charges'

      post '/add_address' => 'profiles#add_address'
      post '/send-reset-password' => 'home#send_reset_password'
      post '/reset-password' => 'home#reset_password'

      post '/gcm-register' => 'home#gcm_register'

      post '/purchase_confirmation' => 'products#purchase_confirmation'
      post '/update_stripe' => 'home#update_stripe'

      get '/bought_for_cause' => 'product_loves#bought_for_cause'

    end
    ###################API version 2.0 Routes Ends Here###################


    ###################API Vesrion 3.0 Routes Starts Here###################
    namespace :v3 do
      #mount_devise_token_auth_for 'User', at: 'auth'
      mount_devise_token_auth_for 'User', at: 'auth', controllers: {
      #passwords:          'api/v2/devise_token_auth/passwords',
      registrations:      'api/v3/devise_token_auth/registrations',
      #sessions:           'api/v3/devise_token_auth/sessions'
      }

      get '/home' => 'home#index'

      resources :products do
        collection do
          get 'filters'
          get 'sorting'
          get 'search_by_query'
          delete '/delete_attachment' => 'products#delete_attachment'
        end
        member do
          get 'other_user_closet'
          put 'set_primary_product_image'
        end
      end

      resources :product_loves
      resources :orders do
        collection do
          post '/return' => 'orders#product_return'
        end
        member do
          post 'product_not_received'
        end
      end
      resources :notifications
      resources :private_messages
      resources :item_comments
      resources :donation_bags
      resources :cashouts
      resources :bank_details
      resources :subscriptions
      resources :profiles do
        member do
          post 'update_password'
          post 'update_return_policy'
          get 'fetch_return_policy'
          post 'update_profile_image'
        end
      end
      get '/types' => 'home#get_types'
      get '/fabrics' => 'home#get_fabrics'
      get '/conditions' => 'home#get_conditions'
      get '/cups' => 'home#get_cups'
      get '/size' => 'home#get_size'
      get '/colors' => 'home#get_colors'
      get '/default_values' => 'home#get_default_values'
      get '/my_closet' => 'products#my_closet'
      get '/sales' => 'orders#my_sales'
      get '/get_address' => 'profiles#get_address'
      get '/get_subscription_and_charges' => 'home#get_subscription_and_charges'

      post '/add_address' => 'profiles#add_address'
      post '/send-reset-password' => 'home#send_reset_password'
      post '/reset-password' => 'home#reset_password'

      post '/gcm-register' => 'home#gcm_register'

      post '/purchase_confirmation' => 'products#purchase_confirmation'
      post '/update_stripe' => 'home#update_stripe'

      get '/bought_for_cause' => 'product_loves#bought_for_cause'

    end
    ###################API version 3.0 Routes Ends Here###################


  end
end
