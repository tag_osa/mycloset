class AddFieldsToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :product_feedback, :text
    add_column :orders, :is_cashout_requested, :boolean
    add_column :orders, :sellers_earnings, :float
  end
end
