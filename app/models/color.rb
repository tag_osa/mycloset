class Color < ActiveRecord::Base
	has_many :products, :through => :color_products
	has_many :color_products, :dependent => :destroy
end
