class AddAddressFieldsToOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :shipping_address_id, :integer, :default => nil
  	add_column :orders, :billing_address_id, :integer, :default => nil
  end
end
