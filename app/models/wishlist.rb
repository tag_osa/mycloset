class Wishlist < ActiveRecord::Base
    belongs_to :product, counter_cache: true
    belongs_to :user

    validates_presence_of :user
    validates_presence_of :product
end
