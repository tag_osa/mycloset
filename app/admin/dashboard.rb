ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    # div class: "blank_slate_container", id: "dashboard_default_message" do
    #   span class: "blank_slate" do
    #     span I18n.t("active_admin.dashboard_welcome.welcome")
    #     small I18n.t("active_admin.dashboard_welcome.call_to_action")
    #   end
    # end

    all_product_types = Type.order("name ASC")
    all_products = Product.order("headline ASC")
    all_users = User.where("id > 0").order("uid ASC")

    # Here is an example of a simple dashboard with columns and panels.
    
    columns do
       column do
        panel "All Users" do
          ul do
            all_users.map do |user|
              li link_to(user.uid, admin_user_path(user))
            end
          end
        end
      end if all_users.present?

      column do
        panel "All Product Types" do
          ul do
            all_product_types.map do |product_type|
              li link_to(product_type.name, admin_product_type_path(product_type))
            end
          end
        end
      end if all_product_types.present?

      column do
        panel "All Products" do
          ul do
            all_products.map do |product|
              li link_to(product.headline, admin_product_path(product))
            end
          end
        end
      end if all_products.present?

    end
  end # content
end
