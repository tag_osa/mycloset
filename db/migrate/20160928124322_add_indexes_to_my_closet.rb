class AddIndexesToMyCloset < ActiveRecord::Migration
  def change
  	add_index :products, :user_id
    add_index :products, :size_id
    add_index :products, :cup_id
    add_index :products, :condition_id
    add_index :products, :fabric_id
    add_index :products, :type_id
    add_index :orders, :user_id
    add_index :item_comments, :product_id
    add_index :private_messages, :user_id
    add_index :private_messages, :receiver_id
    add_index :colors_products, :color_id
    add_index :colors_products, :product_id
    add_index :wishlists, :user_id
    add_index :notifications, :user_id
  end
end
