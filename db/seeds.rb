# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
unless Type.pluck(:id).length > 1
		Type.create(:name=>"Sarees")
		Type.create(:name=>"Lehengas")
		Type.create(:name=>"Anarkalis")
		Type.create(:name=>"Chudidars")
		Type.create(:name=>"Blouses")
		Type.create(:name=>"Accessories")
		Type.create(:name=>"Western")
		Type.create(:name=>"Mens")
		Type.create(:name=>"Childrens")
end

unless Size.pluck(:id).length > 1
		Size.create(:bust=>28)
		Size.create(:bust=>30)
		Size.create(:bust=>32)
		Size.create(:bust=>34)
		Size.create(:bust=>36)
		Size.create(:bust=>38)
		Size.create(:bust=>40)
		Size.create(:bust=>42)
end

unless ShirtSize.pluck(:id).length > 1
		ShirtSize.create(:bust=>'S')
		ShirtSize.create(:bust=>'M')
		ShirtSize.create(:bust=>'L')
		ShirtSize.create(:bust=>'XL')
end

unless Cup.pluck(:id).length > 1
		Cup.create(:name=>"A")
		Cup.create(:name=>"B")
		Cup.create(:name=>"C")
		Cup.create(:name=>"D")
end

unless Color.pluck(:id).length > 1
		Color.create(:name=>"White", :code=>"#FFFFFF")
		Color.create(:name=>"Black", :code=>"#000000")
		Color.create(:name=>"Grey", :code=>"#DFDEDE")
		Color.create(:name=>"Yellow", :code=>"#F8E71C")
		Color.create(:name=>"Orange", :code=>"#F5A623")
		Color.create(:name=>"Pink", :code=>"#F66BE1")
		Color.create(:name=>"Red", :code=>"#D0021B")
		Color.create(:name=>"Green", :code=>"#417505")
		Color.create(:name=>"Blue", :code=>"#4A90E2")
		Color.create(:name=>"Voilet", :code=>"#9013FE")
		Color.create(:name=>"Brown", :code=>"#8B572A")
end

unless Condition.pluck(:id).length > 1
		Condition.create(:name=>"New")
		Condition.create(:name=>"Used - Like New")
		Condition.create(:name=>"Used - Very Good")
		Condition.create(:name=>"Used - Acceptable")
end

unless Fabric.pluck(:id).length > 1
		Fabric.create(:name=>"Silk")
		Fabric.create(:name=>"Brocade")
		Fabric.create(:name=>"Chanderi")
		Fabric.create(:name=>"Chiffon")
		Fabric.create(:name=>"Cotton")
		Fabric.create(:name=>"Crepe")
		Fabric.create(:name=>"Georgette")
		Fabric.create(:name=>"Lycra")
		Fabric.create(:name=>"Net")
		Fabric.create(:name=>"Organza")
		Fabric.create(:name=>"Pashmina")
		Fabric.create(:name=>"Satin")
		Fabric.create(:name=>"Tissue")
		Fabric.create(:name=>"Velvet")
end

unless Subscription.pluck(:id).length > 1
		Subscription.create(:name=>"Subscriber Pack", :amount => 29.99, :status => true, :months => 12)
end

admin_user = AdminUser.find_or_create_by(:email=>"admin@mycloset.style")
admin_user.update_attributes(:password=>"password",:password_confirmation=>"password")