class AddFieldsToOrdersTable < ActiveRecord::Migration
  def change
  	add_column :orders, :shipped_on, :date
  	add_column :orders, :transfer_id, :string
  	add_column :orders, :transfer_date, :datetime
  end
end
