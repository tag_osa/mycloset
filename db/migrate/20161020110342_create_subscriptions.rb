class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.string :name
      t.float :amount
      t.boolean :status, :default=>true
      t.integer :months

      t.timestamps null: false
    end
  end
end
