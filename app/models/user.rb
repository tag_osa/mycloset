class User < ActiveRecord::Base
 has_many :billing_addresses
 has_many :shipping_addresses
 has_many :products
 has_many :wishlists
 has_many :notifications
 has_many :private_messages
 has_many :user_regids
 has_one :seller_return_policy
 has_many :cashouts
 has_many :orders
 has_one :seller_bank_detail
 has_one :attachment, :as => :attachable, :dependent => :destroy
 has_many :user_subscriptions
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :omniauthable
  include DeviseTokenAuth::Concerns::User

  after_create :create_user_return_policy

  def create_user_return_policy
    self.create_seller_return_policy(
        :is_allow => true,
        :is_pay_for_return_policy => true,
        :notify_days => 5
    )
  end

  # Fetch user`s notifications count
  def notifications_count
    self.notifications.where(:status => false).count()
  end

  def is_user_subscribed
    self.user_subscriptions.where(:status => true).empty? ? false : true
  end

  # Fetch user`s private messages count
  def self.private_messages_count(user)
    PrivateMessage.where(:is_read => false,:receiver_id=>user.id).count()
  end

end
