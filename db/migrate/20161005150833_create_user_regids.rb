class CreateUserRegids < ActiveRecord::Migration
  def change
    create_table :user_regids do |t|
      t.text :reg_id
      t.string :device_os
      t.boolean :status, default: true
      t.references :user
      t.timestamps null: false
    end
  end
end
