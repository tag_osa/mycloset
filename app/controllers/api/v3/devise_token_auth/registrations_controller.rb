class Api::V3::DeviseTokenAuth::RegistrationsController < DeviseTokenAuth::RegistrationsController
    include Devise::Controllers::Helpers
	def create
	  	if params[:provider] == "facebook" || params[:provider] == "google"
	  		@resource = User.find_by_email(params[:email]) || User.find_by_uid(params[:uid])
	  		if @resource
	  			# create client id and token
				client_id = SecureRandom.urlsafe_base64(nil, false)
				token     = SecureRandom.urlsafe_base64(nil, false)

				# store client + token in user's token hash
				@resource.tokens[client_id] = {
				  token: BCrypt::Password.create(token),
				  expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
				}

				# generate auth headers for response
				new_auth_header = @resource.build_auth_header(token, client_id)

				# update response with the header that will be required by the next request
				response.headers.merge!(new_auth_header)
	  			render_create_success
	  		else
		  		@resource = User.create(user_params)
		  		# create client id and token
				client_id = SecureRandom.urlsafe_base64(nil, false)
				token     = SecureRandom.urlsafe_base64(nil, false)
				UserMailer.send_welcome_email(@resource).deliver
				# store client + token in user's token hash
				@resource.tokens[client_id] = {
				  token: BCrypt::Password.create(token),
				  expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
				}

				# generate auth headers for response
				new_auth_header = @resource.build_auth_header(token, client_id)

				# update response with the header that will be required by the next request
				response.headers.merge!(new_auth_header)
		  		@resource.errors.present? ? render_create_error : render_create_success
	  		end
	  		
	  	else
		    super do |resource|
		    	resource.update_attributes(:name => params[:name])
		    	UserMailer.send_welcome_email(resource).deliver
		    	resource
		    end
	    end
	end

  	private

	def user_params
	    params.require(:registration).permit(
	        :email,
	        :password, 
	        :password_confirmation,
	        :name,
	        :nickname,
	        :image,
	        :user_type,
	        :provider,
	        :uid
	    )
	end

end