class Api::V3::BankDetailsController < ApplicationController
    include DeviseTokenAuth::Concerns::SetUserByToken
    before_action :authenticate_api_v1_user!

    # This method used for create user(seller) bank details
    def create
        begin
            @bank_detail = current_api_v1_user.seller_bank_detail
            if @bank_detail.nil?
                params[:bank_details][:user_id] = current_api_v1_user.id
                @bank_detail = SellerBankDetail.create(
                    bank_details_params
                )
            else
                @bank_detail.update_attributes(bank_details_params)
            end
            current_api_v1_user.update_attributes(user_details_params)
            common_response
        rescue Exception => e
            p "--ERROR--"
            p e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # This method used for get user(seller) bank details
    def show
        begin
            @bank_detail = SellerBankDetail.find_by_user_id(current_api_v1_user.id)            
            common_response
        rescue Exception => e
            p "--ERROR--"
            p e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    def common_response
        json_response = Hash.new
        json_response["bank_detail"] = @bank_detail || BankDetail.new
        json_response["user"] = current_api_v1_user.attributes.slice("first_name","last_name","dob_day","dob_month","dob_year")
        render success_response_with_object("sucess", 200, json_response)
    end

    private
    def bank_details_params
        params.require(:bank_details).permit!
    end

    def user_details_params
        params.require(:user).permit!
    end
end
