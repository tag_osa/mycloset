class Notification < ActiveRecord::Base
	belongs_to :product
	belongs_to :user


	#Rpush Initialize the android notification for first time only
	#Run this Method from rails console
	#Steps:In production
	#rails c
	#Notification.android_push_notification_setup()
	def self.android_push_notification_setup
		p "Android Push Notification"	    
	    app = Rpush::Gcm::App.new
	    app.name = "android_app"
	    #lathsharma@mycloset
	    #app.auth_key = "AIzaSyBxsH4jN_H0nW-_S2RuLHi8zEy2ZPxMr7k"
	    #admin@mycloset
	    app.auth_key = "AIzaSyAxNDz4ORN-7--2pGuYNvZ17DgCJHogw1k"
	    app.connections = 1
	    app.save!
	    p "Android Push Notification Setup Completed Successfuly"
	end

	#Rpush Initialize the android notification for first time only
	#we need .pem file and certificate password
	#Run this Method from rails console
	#Steps:In production
	#rails c
	#Notification.ios_push_notification_setup()
	def self.ios_push_notification_setup
		p "IOS Push Notification Setup Process Started"
		file = File.join(Rails.root, 'config','development.pem')
		File.read(file)
		#File.read(File.dirname(Rails.root)+"/rails_mycloset/config/development.pem")
		app = Rpush::Apns::App.new
	    app.name = "ios_app"
	    app.certificate = File.read(file)
	    #app.certificate = File.read("/path/to/development.pem")
	    app.environment = "development" # APNs environment.
	    app.password = ""
	    app.connections = 1
	    app.save!
	    p "IOS Push Notification Setup Completed Successfuly"
	end
end
